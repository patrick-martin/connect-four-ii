const player1 = function(element) {
    element.style.background = 'red';
    console.log('player 1 moves to ' + element.id)
}
const player2 = function(element) {
    element.style.background = 'black';
    console.log('player 2 moves to ' + element.id)
}

let start = false;

function makeMove(element) {
    if (!start) {
        start = true;
        player1(element);
    } else {
        start = false;
        player2(element);
    }
}

//Setting an addEventListener to each individual cell, rather than using 'onclick' on each element in HTML
//Doesn't seem to want to work for some reason!
// const tableCells = document.querySelectorAll('.cells');
// tableCells.addEventListener('click', makeMove(this));